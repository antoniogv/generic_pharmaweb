/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('genePharma')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
