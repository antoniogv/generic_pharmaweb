(function () {
  'use strict';
  angular
    .module('dashboard.navside')
    .controller('NavSideController', NavSideController);

  function NavSideController() {
    var vm = this;

    vm.textMenu = 'Este es el menu vertical';
  }
})();
