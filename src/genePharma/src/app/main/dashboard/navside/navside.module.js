(function () {
  'use strict';

  angular
    .module('dashboard.navside', [])
    .config(config);

  function config($routeProvider) {
    $routeProvider
      .when('/menu', {
        templateUrl: 'app/main/dashboard/navside/navside.html',
        controller: 'NavSideController',
        controllerAs: 'vm'
      })
  }
})();
