(function() {
  'use strict';

  angular
    .module('genePharma', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 
    'ngMessages', 'ngAria', 'restangular', 'ngRoute', 'ngMaterial', 'toastr', 'dashboard']);

})();
