(function() {
  'use strict';

  angular
    .module('genePharma')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
